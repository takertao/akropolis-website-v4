export { default as PRIVACY_POLICY_URL } from 'assets/Akropolis_Privacy_Policy.pdf';
export { default as T_AND_C_URL } from 'assets/Akropolis_Terms_and_Conditions.pdf';
export { default as QUEST_T_AND_C_URL } from 'assets/Akropolis_Terms_and_Conditions_Quest_3.pdf';
export { default as BOUNTY_T_AND_C_URL } from 'assets/Akropolis_Terms_and_Conditions_Bounty.pdf';
export { default as TOKEN_SWAP_T_AND_C_URL } from 'assets/Akropolis_Terms_and_Conditions_Token_Swap.pdf';
