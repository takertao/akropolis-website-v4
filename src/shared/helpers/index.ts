export { default as withComponent } from './withComponent';
export { default as makeFeatureEntry } from './makeFeatureEntry';
export { default as inIframe } from './inIframe';
export * from './style';
